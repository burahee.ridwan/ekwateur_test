<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Service\OfferService;

class OfferServiceTest extends KernelTestCase
{
    /**
     * Test case where given code has offers related
     */
    public function testCodeHasOffers(): void
    {
        self::bootKernel();

        $container = self::$container;

        $offerService = $container->get(OfferService::class);
        $result = $offerService->getAllOfferForPromoCode('ELEC_N_WOOD');

        $this->assertNotEmpty($result);
    }

    /**
     * Test case where given code has no offers related
     */
    public function testCodeHasNoOffers(): void
    {
        self::bootKernel();

        $container = self::$container;

        $offerService = $container->get(OfferService::class);
        $result = $offerService->getAllOfferForPromoCode('CODE_WITH_NO_OFFER');

        $this->assertEmpty($result);
    }

    /**
     * Test case where given code is empty
     */
    public function testCodeIsEmpty(): void
    {
        self::bootKernel();

        $container = self::$container;

        $offerService = $container->get(OfferService::class);
        $result = $offerService->getAllOfferForPromoCode('');

        $this->assertEmpty($result);
    }
}