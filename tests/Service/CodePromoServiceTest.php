<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Service\CodePromoService;

class CodePromoServiceTest extends KernelTestCase
{
    /**
     * Test case where given code exist and is valid
     */
    public function testCodeIsValid(): void
    {
       self::bootKernel();

        $container = self::$container;

        $codePromoService = $container->get(CodePromoService::class);
        $result = $codePromoService->isPromoCodeValid('ELEC_N_WOOD');

        $this->assertTrue($result);
    }

    /**
     * Test case where given code exist and is invalid
     */
    public function testCodeExistAndIsInvalid(): void
    {
        self::bootKernel();

        $container = self::$container;

        $codePromoService = $container->get(CodePromoService::class);
        $result = $codePromoService->isPromoCodeValid('EKWA_WELCOME');

        $this->assertFalse($result);
    }

    /**
     * Test case where given code not exist
     */
    public function testCodeNotExist(): void
    {
        self::bootKernel();

        $container = self::$container;

        $codePromoService = $container->get(CodePromoService::class);
        $result = $codePromoService->isPromoCodeValid('NOT_EXIST');

        $this->assertFalse($result);
    }

    /**
     * Test case where given code is empty
     */
    public function testCodeEmpty(): void
    {
        self::bootKernel();

        $container = self::$container;

        $codePromoService = $container->get(CodePromoService::class);
        $result = $codePromoService->isPromoCodeValid('');

        $this->assertFalse($result);
    }
}