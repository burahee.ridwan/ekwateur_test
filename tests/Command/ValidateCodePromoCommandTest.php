<?php

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use App\Command\ValidateCodePromoCommand;
use App\Service\CodePromoService;
use App\Service\OfferService;

class ValidateCodePromoCommandTest extends KernelTestCase
{
    /**
     * Mock for CodePromoService
     */
    protected $mockCodePromoService;

    /**
     * Mock for OfferService
     */
    protected $mockOfferService;

    public function setUp(): void
    {
        $this->mockCodePromoService = $this->getMockBuilder(CodePromoService::class)
        ->setMethods([
            'isPromoCodeValid'
        ])
        ->disableOriginalConstructor()
        ->getMock();

        $this->mockOfferService = $this->getMockBuilder(OfferService::class)
        ->setMethods([
            'getAllOfferForPromoCode'
        ])
        ->disableOriginalConstructor()
        ->getMock();
    }

    /**
     * Test case where given code is valid and result is a json file with all finded offers
     */
    public function testCodePromoValidWithOffer(): void
    {
        // Prepare mock 
        $promoCode = 'ELEC_N_WOOD';

        $this->mockCodePromoService->expects($this->once())
        ->method('isPromoCodeValid')
        ->with($promoCode)
        ->willReturn(true);

        $this->mockOfferService->expects($this->once())
        ->method('getAllOfferForPromoCode')
        ->with($promoCode )
        ->willReturn([
            'offerType' => 'GAS',
            'offerName' => 'EKWAG2000',
            'offerDescription' => 'Une offre incroyable',
            'validPromoCodeList' => array($promoCode ,'ALL_2000')
        ]);

        $kernel = static::createKernel();
        $application = new Application($kernel);
        $application->add(new ValidateCodePromoCommand($this->mockCodePromoService, $this->mockOfferService));

        $command = $application->find('promo-code:validate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'code_promo' => $promoCode
        ]);
        
        // Test if file was created
        $this->assertTrue(file_exists($promoCode.'_related_offers.json'));
        
        // propertie to save offers read in json file
        $jsonEncodedString = '';
        $filestream = false;

        try{
            // Get offers in file
            $filestream = fopen($promoCode. '_related_offers.json','r');
            
            while(($offer = fgets($filestream, 1000)) !== false)
            {
                $jsonEncodedString .= $offer;
            }
        }
        catch(\Exception $e)
        {
            // Error when reading json file
        }
        finally
        {
            fclose($filestream);
        }

        // Test offer value
        $offersDecoded = json_decode($jsonEncodedString);
        $this->assertEquals('GAS', $offersDecoded->offerType);
        $this->assertEquals('EKWAG2000', $offersDecoded->offerName);
        $this->assertEquals('Une offre incroyable', $offersDecoded->offerDescription);
        $this->assertEquals(2, count($offersDecoded->validPromoCodeList));

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Promo code has offer(s) linked. File '.$promoCode. '_related_offers.json created', $output);
    }

    /**
     * Test case where given code is valid but no offers are related
     */
    public function testCodePromoValidWithNoOffer(): void
    {
        // Prepare mock 
        $promoCode = 'CODE_WITH_NO_OFFER';

        $this->mockCodePromoService->expects($this->once())
        ->method('isPromoCodeValid')
        ->with($promoCode)
        ->willReturn(true);

        $this->mockOfferService->expects($this->once())
        ->method('getAllOfferForPromoCode')
        ->with($promoCode )
        ->willReturn([]);

        $kernel = static::createKernel();
        $application = new Application($kernel);
        $application->add(new ValidateCodePromoCommand($this->mockCodePromoService, $this->mockOfferService));

        $command = $application->find('promo-code:validate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'code_promo' => $promoCode
        ]);
        
        // Test if file was created
        $this->assertFalse(file_exists($promoCode.'.json'));

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Promo code \''.$promoCode.'\' has no offer related', $output);
    }

    /**
     * Test case where given code exist but is not valid
     */
    public function testCodePromoNotValid(): void
    {
        // Prepare mock 
        $promoCode = 'EKWA_WELCOME';

        $this->mockCodePromoService->expects($this->once())
        ->method('isPromoCodeValid')
        ->with($promoCode)
        ->willReturn(false);

        $kernel = static::createKernel();
        $application = new Application($kernel);
        $application->add(new ValidateCodePromoCommand($this->mockCodePromoService, $this->mockOfferService));

        $command = $application->find('promo-code:validate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'code_promo' => $promoCode
        ]);
        
        // Test if file was created
        $this->assertFalse(file_exists($promoCode.'.json'));

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Promo code \''.$promoCode.'\' is not valid', $output);
    }

    /**
     * Test case where no code is given
     */
    public function testMissingArgumentError(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Not enough arguments (missing: "code_promo").');

        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('promo-code:validate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);

        $output = $commandTester->getDisplay();
    }
}