<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\CodePromoService;
use App\Service\OfferService;

class ValidateCodePromoCommand extends Command
{
    // The name of the command
    protected static $defaultName = 'promo-code:validate';

    /**
     * Constructor
     * Using the PHP8 new constructor syntax to define codePromoService and offerService properties
     * @param CodePromoService $codePromoService
     * @param OfferService $offerService
     */
    public function __construct(protected CodePromoService $codePromoService, protected OfferService $offerService)
    {
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this->setDescription('Validate the given code promo')
        ->addArgument('code_promo', InputArgument::REQUIRED , 'Code promo');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $outputFileStream = false;
        $promoCode = $input->getArgument('code_promo');
        $isPromoCodeValid = $this->codePromoService->isPromoCodeValid($promoCode);

        // If code is valid then we search for offers related to it
        if($isPromoCodeValid)
        {
            $relatedOfferList = $this->offerService->getAllOfferForPromoCode($promoCode);

            // If offers are find, we save them into a json file
            if(count($relatedOfferList) > 0)
            {
                try
                {
                    $outputFileStream = fopen($promoCode . '_related_offers.json', 'w');
                    fputs($outputFileStream, json_encode($relatedOfferList));
                    
                    $output->writeln('Promo code has offer(s) linked. File '.$promoCode. '_related_offers.json created');
                }
                catch(\Exception $e)
                {
                    $output->writeln($e->getMessage());
                    $output->writeln($e->getTraceAsString());
                }
                finally
                {
                    fclose($outputFileStream);
                }
            }else{
                $output->writeln('Promo code \''.$promoCode.'\' has no offer related');
            }
        }else{
            $output->writeln('Promo code \''.$promoCode.'\' is not valid');
        }
        
        return Command::SUCCESS;
    }
}
