<?php 

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Service used to get promo code list
 */
class CodePromoService 
{
    /**
     * @var array $codePromoList List which contains all promo code finded
     */
    private array $codePromoList;

    public function __construct(private HttpClientInterface $client)
    {
        $this->codePromoList = $this->getAllCodePromo();
    }

    /**
     * Verify if a given code is valid or not
     * A promo code is valid if it's endDate is not passed or if code do not exist
     * @param string $promoCode promo code to valid
     * @return bool true if promo code is valid
     */
    public function isPromoCodeValid(string $promoCode = ''): bool
    {
        $result = false;

        // If no promo code is given, return false
        if($promoCode === '')
        {
            return $result;
        }

        foreach($this->codePromoList as $p)
        {
            // If we find a match for given promo code, then we test endDate
            if($p['code'] === $promoCode)
            {
                $endDate = new \DateTime($p['endDate']);
                $now = new \DateTime();

                // If endDate is passed, the code is not valid
                $result = $endDate->getTimestamp() >= $now->getTimestamp();

                break;
            }
        }

        return $result;
    }

    /**
     * Get all promo code
     * @return array all promo code
     */
    private function getAllCodePromo(): array
    {
        $response = $this->client->request(
            'GET',
            'https://601025826c21e10017050013.mockapi.io/ekwatest/promoCodeList'
        );

        $statusCode = $response->getStatusCode();
        $contentType = $response->getHeaders()['content-type'][0];
        $content = $response->getContent();
        $content = $response->toArray();

        return $content;
    }
}