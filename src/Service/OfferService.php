<?php 

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Service used to get offer list
 */
class OfferService
{
    /**
     * @var array $offerList List which contains all offers finded
     */
    private array $offerList;

    public function __construct(private HttpClientInterface $client)
    {
        $this->offerList = $this->getAllOffer();
    }

    /**
     * Get all offers related to a given promo code
     * @param string $promoCode code to search in offers list
     * @return array all offers related to given promo code
     */
    public function getAllOfferForPromoCode(string $promoCode = ''): array
    {
        $result = array();

        // If given promo code is empty
        if($promoCode === '')
        {
            return $result;
        }

        foreach($this->offerList as $offer)
        {
            // If given promo code is in validPromoCodeList then we save the current offer
            if(in_array($promoCode, $offer['validPromoCodeList'])){
                $result[] = $offer;
            }
        }

        return $result;
    }

    /**
     * Get all offer
     * @return array all offer finded
     */
    private function getAllOffer(): array
    {
        $response = $this->client->request(
            'GET',
            'https://601025826c21e10017050013.mockapi.io/ekwatest/offerList'
        );

        $statusCode = $response->getStatusCode();
        $contentType = $response->getHeaders()['content-type'][0];
        $content = $response->getContent();
        $content = $response->toArray();

        return $content;
    }
}